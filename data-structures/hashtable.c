#include <stdio.h>
#include <stdlib.h>

#define SIZE 20

struct DataItem {
  int data;
  int key;
};

struct DataItem *hashArray[SIZE];
struct DataItem *dummyItem;
struct DataItem *Item;

int make_hash(int key) {
  return key % SIZE;
}

struct DataItem *search(int key) {
  int hash = make_hash(key);
  while(hashArray[hash] != NULL) {
    if(hashArray[hash]->key == key) {
      return hashArray[hash];
    }
    hash++;
    hash %= SIZE;
  }
  return NULL;
}

struct DataItem *delete(struct DataItem *item) {
  int key = item->key;
  int hash = make_hash(key);

  while(hashArray[hash] != NULL) {
    if(hashArray[hash]->key == key) {
      struct DataItem *temp = hashArray[hash];
      hashArray[hash] = dummyItem;
      return temp;
    }
    hash++;
    hash %= SIZE;
  }

  return NULL;
}

void insert(int key, int data) {
  struct DataItem *item = (struct DataItem*) malloc(sizeof(struct DataItem));
  item->key = key;
  item->data = data;

  int hash = make_hash(key);
  while(hashArray[hash] != NULL && hashArray[hash]->key != -1) {
    hash++;
    hash %= SIZE;
  }
  hashArray[hash] = item;
}

void display() {
  for(int i=0; i<SIZE; i++) {
    if(hashArray[i] != NULL) {
      printf(" (%d, %d)", hashArray[i]->key, hashArray[i]->data);
    }
    else {
      printf("-----");
    }
  printf("\n");
  }
}

void main() {
  struct DataItem* item;
  dummyItem = (struct DataItem*) malloc(sizeof(struct DataItem));
  dummyItem->key = -1;
  dummyItem->data = -1;

  insert(1, 20);
  insert(2, 70);
  insert(42, 80);
  insert(4, 25);
  insert(12, 44);
  insert(14, 32);
  insert(17, 11);
  insert(13, 78);
  insert(37, 97);

  display();

  item = search(37);
  printf("Item found: %d\n", item->data);

  delete(item);
  item = search(37);
  if(item == NULL) {
    printf("Item didn't find\n");
  }
  display();
}
